<?php
  class tijdklok {
    private $sql_conn;
    function __construct($sql) { $this->sql_conn = $sql; }

    public function setEnable($id, $value) {
      $query = "UPDATE `tijdklok` SET `tk_enable`='".$value."' where `tk_dag_nr`='".$id."'";
  		$result = $this->sql_conn->query($query);
  		if (!is_int($result)) { return "Couldn't update TK: ".$result; }
  		return FALSE;
    }

    public function setTemp($id, $temp) {
      $query = "UPDATE `tijdklok` SET `tk_temp`='".$temp."' where `tk_dag_nr`='".$id."'";
  		$result = $this->sql_conn->query($query);
  		if (!is_int($result)) { return "Couldn't update TK: ".$result; }
  		return FALSE;
    }

    public function setTijd($id, $tijd) {
      $query = "UPDATE `tijdklok` SET `tk_tijd`='".$tijd."' where `tk_dag_nr`='".$id."'";
  		$result = $this->sql_conn->query($query);
  		if (!is_int($result)) { return "Couldn't update TK: ".$result; }
  		return FALSE;
    }

?>
