<?php

require_once($_SERVER["DOCUMENT_ROOT"]."/includes/config.php");

class sql {
	private $mysqli;

	function __construct() {
		$this->mysqli = new mysqli();
		$this->mysqli->init();
		if (!$this->mysqli) {
			die('mysqli_init failed');
		}

		if (!$this->mysqli->real_connect(SQL_HOST, SQL_USER, SQL_PASSWORD, SQL_DATABASE)) {
			die('Connect Error (' . mysqli_connect_errno() . '): '.mysqli_connect_error());
		}
	}

	public function query($query) {
		$command = strtoupper(substr($query,0,stripos($query, " ")));

		if (!$this->mysqli->real_query($query)) {
			$error = "ERROR: ".$this->mysqli->error."\n";
			return $error;
		} else {
			if ($command == "SELECT") {
				$result = $this->mysqli->store_result();
				if($this->mysqli->affected_rows == 0) {
					// No result was found
					$result->free();
					return FALSE;
				}
				else if($this->mysqli->affected_rows > 0) {
					// multiple results are returned
					$i = 0;
					while ($row = $result->fetch_assoc())
					{
						 $output[$i] = $row;
						 $i++;
					}
					$result->free();
					return $output;
				} else {
					// this should never happen, ERROR!
					$error = "ERROR: ".$this->mysqli->error."\n";
					$result->free();
					return $error;
				}
			} elseif ($command == "INSERT") {
				// INSERT is done. Fetch the latest insert id
				$return = array('id' => $this->mysqli->insert_id);
				return $return;
			} else {
				// No SELECT or INSERT was done
				$affected = $this->mysqli->affected_rows;
				if ($affected > 0) {
					// rows changed, return affected row number
					return $affected;
				} else {
					// nothing changed, return false
					return FALSE;
				}
			}
		}
	}

	public function constructInsert($table, $input, $usekey = FALSE, $filter = NULL) {
		if(!is_array($input)) { return FALSE; }
		$output = "INSERT INTO `".$table."` ";
		if($usekey === TRUE) {
			$output .= " (";
			$i = 0;
			foreach($input as $key => $value) {
				// check the filter
				if(!is_array($filter) || !in_array($key, $filter)) {
					if($i > 0) { $output .= ","; }
					$output .= "`".$key."`";
					$i++;
				}
			}
			$output .= ") ";
			reset($input);
		}
		$output .= "VALUES ";
		$output .= " (";
		$i = 0;
		foreach($input as $key => $value) {
			// check the filter
			if(!is_array($filter) || !in_array($key, $filter)) {
				if($i > 0) { $output .= ","; }
				// check types
				if($value == NULL || $value == "" || strtolower($value) == 'null') {
					$output .= "NULL";
				} elseif (is_float($value)) {
					$output .= "'".$value."'";
				} elseif (is_int($value)) {
					$output .= $value;
				} else {
					$output .= "'".$value."'";
				}
				$i++;
			}
		}
		$output .= ")";
		return $output;
	}

	public function constructIsIn($array) {
		if(!is_array($array)) { return FALSE; }
		$i = 0;
		foreach($array as $value) {
			if($i > 0) { $output .= ","; }
			$output .= $value;
			$i++;
		}
		return $output;
	}

	public function constructUpdate($table, $input, $where, $filter = NULL) {
		if(!is_array($input) || !$where) { return FALSE; }
		$output = "UPDATE `".$table."` SET ";
		$i = 0;
		foreach($input as $key => $value) {
			// check the filter
			if(!is_array($filter) || !in_array($key, $filter)) {
				if($i > 0) { $output .= ","; }
				// check types
				if($value == NULL || $value == "" || strtolower($value) == 'null') {
					$output .= '`'.$key."`=NULL";
				} elseif (is_float($value)) {
					$output .= '`'.$key."`='".$value."'";
				} elseif (is_int($value)) {
					$output .= '`'.$key."`=".$value;
				} else {
					$output .= '`'.$key."`='".$value."'";
				}
				$i++;
			}
		}
		$output .= " WHERE ".$where;
		return $output;
	}

	public function escape($input) {
		if(is_array($input)) {
			foreach($input as $key => $value) {
				$output[$key] = $this->escape($value);
			}
		} else {
			$output = $this->mysqli->real_escape_string($input);
		}
		return $output;
	}

	public function disconnect() {
		$this->mysqli->close();
		echo "Closing SQL\n";
		return TRUE;
	}
}

?>
