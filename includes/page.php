<?php

class page {
  public $sql_conn;
  private $footer;

  function __construct($auth = TRUE, $sql = TRUE, $header = TRUE, $footer = TRUE) {
		header('Content-Type: text/html; charset=UTF-8');

		if($sql === TRUE) {
			$this->sql_conn = new sql;
		}

    if($auth === TRUE) {
      $this->auth();
    }

		if($header === TRUE) {
			$this->header();
		}

    $this->footer = $footer;
  }

	public function header() {
		require_once($_SERVER["DOCUMENT_ROOT"]."/templates/header.php");
	}

  private function auth() {
		global $S_data;
		session_start();
    unset($session_status); unset($session);

    $session = new session($this->sql_conn);
    $session_status = $session->auth($_SESSION["ID"], $_SESSION["Token"]);
    if($session_status) {
    	if($session_status == "Verified") {
    		// Authentication verified
    		unset($renew);
    		$renew = $session->renew($_SESSION["ID"], $_SESSION["Token"]);
    		if (!$renew) {
    			// Renew worked
    			$S_data = $session->getData($_SESSION["ID"], $_SESSION["Token"]);
    		} else {
    			// Renew failed
    			$this->redirect('login.php?MSG='.urlencode($renew));
    			die("<br><br><br><br><center><h1>Oops! A crash in page Auth #1!</h1></center>");
    		}
    	} else {
    		// An error was thrown by AuthSession, return to login page after closing the session
    		$session->close($_SESSION["ID"], $_SESSION["Token"]);
    		$this->redirect('login.php?MSG='.urlencode($session_status));
    		die("<br><br><br><br><center><h1>Oops! A crash in page Auth #2!</h1></center>");
    	}
    } else {
    	// First visit or auth somehow failed utterly, return to login page without message
    	$this->redirect('login.php');
    	die("<br><br><br><br><center><h1>Oops! A crash in page Auth #3!</h1></center>");
    }
  }

  public function redirect($page) {
    if(strtolower($_SERVER["HTTPS"]) == "off") { $protocol = 'http'; } else { $protocol = 'https'; }
    // in some cases HTTPS is not declared and REQUEST_SCHEME can be found to be more acurate
    if(!$_SERVER["HTTPS"] && $_SERVER["REQUEST_SCHEME"] == "http") { $protocol = 'http'; }
    header('Location: '.$protocol.'://'.$_SERVER["SERVER_NAME"].'/'.$page);
  }

  function __destruct() {
		if(!stristr($_SERVER['PHP_SELF'], "/queries")) {
      if($this->footer === TRUE) {
        require_once($_SERVER["DOCUMENT_ROOT"]."/templates/footer.php");
      }
		}
	}
}

 ?>
