<?php

ini_set("display_errors", '1');
require_once($_SERVER["DOCUMENT_ROOT"].'/includes/page.php');
require_once($_SERVER["DOCUMENT_ROOT"].'/includes/sql.php');
require_once($_SERVER["DOCUMENT_ROOT"].'/includes/table.php');
spl_autoload_extensions('.php');
spl_autoload_register();

function getDateArray() {
  return array(
    1 => "ma",
    2 => "di",
    3 => "wo",
    4 => "do",
    5 => "vr",
    6 => "za",
    7 => "zo"
  );
}

 ?>
