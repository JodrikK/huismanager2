<?php
  class table {
    private $sql_conn;
    function __construct($sql) { $this->sql_conn = $sql; }

    public function getTijdKlok($day) {
      $query = "SELECT * FROM `tijdklok`
                WHERE `tk_dag_nr` LIKE '".$day."_%' ORDER BY `tk_dag_nr` ASC";
      $result = $this->sql_conn->query($query);

      return $result;
    }

    public function renderTable($day, $number = 6) {
      $times = $this->getTijdKlok($day);
      echo '<table class="is-striped"><tbody>';
      foreach ($times as $time) {
        echo '<tr>';
        if($time["tk_enable"] == "1") { $checked = 'checked="checked"'; } else { unset($checked); }
        echo '<td><input id="'.$time["tk_dag_nr"].'-enable" type="checkbox" '.$checked.' onchange="tkEnable(\''.$time["tk_dag_nr"].'\')"></td>';
        echo '<td><input id="'.$time["tk_dag_nr"].'-temp" type="text" placeholder="temp" value="'.$time["tk_temp"].'" onchange="tkSetTemp(\''.$time["tk_dag_nr"].'\')"></td>';
        echo '<td><input id="'.$time["tk_dag_nr"].'-tijd" type="text" placeholder="tijd" value="'.substr($time["tk_tijd"],0,5).'" onchange="tkSetTime(\''.$time["tk_dag_nr"].'\')"></td>';
        echo '</tr>';
      }
      echo '</tbody></table>';
    }
  }
?>
