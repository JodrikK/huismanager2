<?php
	require_once($_SERVER["DOCUMENT_ROOT"].'/includes/base.php');
	$page = new page(FALSE);
  $table = new table($page->sql_conn);
?>
<div class="is-row">
  <div class="is-col">
    <b>Maandag</b>
    <?php $table->renderTable("ma"); ?>
  </div>
  <div class="is-col">
    <b>Dinsdag</b>
    <?php $table->renderTable("di"); ?>
  </div>
  <div class="is-col">
    <b>Woensdag</b>
    <?php $table->renderTable("wo"); ?>
  </div>
  <div class="is-col">
    <b>Donderdag</b>
    <?php $table->renderTable("do"); ?>
  </div>
  <div class="is-col">
    <b>Vrijdag</b>
    <?php $table->renderTable("vr"); ?>
  </div>
  <div class="is-col">
    <b>Zaterdag</b>
    <?php $table->renderTable("za"); ?>
  </div>
  <div class="is-col">
    <b>Zondag</b>
    <?php $table->renderTable("zo"); ?>
  </div>
</div>
