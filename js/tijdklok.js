var id;
var dag;

function tkEnable(id) {
  var value = document.getElementById(id+'-enable').checked;
  if(value === true) { value = 1; } else { value = 0; }
  console.log('Updating enable '+id+'-enable');

  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("txtHint").innerHTML = this.responseText;
    }
  };
  xmlhttp.open("POST", "/queries/tk_update_enable.php?id="+id+"&value="+value, true);
  xmlhttp.send();

}

function tkSetTemp(id) {
  var value = document.getElementById(id+'-temp').value;
  console.log('Updating temp '+id+'-temp');

  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("txtHint").innerHTML = this.responseText;
    }
  };
  xmlhttp.open("POST", "/queries/tk_update_temp.php?id="+id+"&value="+value, true);
  xmlhttp.send();

}

function tkSetTime(id) {
  var value = document.getElementById(id+'-tijd').value;
  console.log('Updating tijd '+id+'-tijd');

  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("txtHint").innerHTML = this.responseText;
    }
  };
  xmlhttp.open("POST", "/queries/tk_update_tijd.php?id="+id+"&value="+value, true);
  xmlhttp.send();

}

function getTijdKlok(dag) {
  var xmlhttp = new XMLHttpRequest();
  var result;
  xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          result = JSON.parse(this.responseText);
          if(typeof result !== "undefined") {
            renderTable(result);
          }
      }
  };
  xmlhttp.open("GET", "/queries/get_get_date.php?&date="+dag, true);
  xmlhttp.send();
}

function renderTable() {
  var table = document.getElementById(tableid);
  var rowCount = table.rows.length;

  var cell0 = newRow.insertCell(0);
  var cell1 = newRow.insertCell(1);
  var cell1 = newRow.insertCell(1);

  cell0.innerHTML = '<div class=\"input-groups\"><span class=\"input-prepend\" onclick=\"remRow(\''+tableid+'\',this)\"><i class=\"fa fa-times\"></i></span><input type=\"text\" name=\"price[]\" id=\"price'+row+'\" required=\"required\" onchange=\"taxSubTotal('+row+')\" /><span class="input-append" onclick="deductTax(\''+tableid+'\', '+row+')"><i class="fa fa-undo"></i></span></div>';
  cell1.innerHTML = '<input type=\"text\" name=\"tax[]\" id=\"tax'+row+'\" class=\"tax width-100\" required=\"required\" onchange=\"taxSubTotal('+row+')\" /><input class=\"subtotal\" type=\"hidden\" name=\"subtotal[]\" id=\"subtotal'+row+'\" />';
  cell1.colSpan = 2;
}
