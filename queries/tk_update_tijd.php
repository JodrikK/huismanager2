<?php
require_once($_SERVER["DOCUMENT_ROOT"].'/includes/base.php');
$page = new page(FALSE, TRUE, FALSE, FALSE);
$id = $page->sql_conn->escape($_REQUEST['id']);
$value = $page->sql_conn->escape($_REQUEST['value']);

if($id && $value) {
	$query = $page->sql_conn->constructUpdate('tijdklok', array("tk_tijd" => $value), "`tk_dag_nr`='".$id."'");
	$result = $page->sql_conn->query($query);
	if(!is_int($result)) { echo "Failed to update TK $id: ".$result; } else { echo "Updated"; }
}

?>
